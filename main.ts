import {
	GetProductsForIngredient,
	GetRecipes,
} from "./supporting-files/data-access";
import { NutrientFact, Product } from "./supporting-files/models";
import {
	GetCostPerBaseUnit,
	GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

//* ITERATE THROUGH RECIPES IN THE TABLE
for (const recipe of recipeData) {
	//* DECLARE INITIAL VALUES
	let cheapestRecipeCost = 0;
	const nutrientsAtCheapestCost: { [key: string]: NutrientFact } = {};

	//* ITERATE THROUGH INGREDIENTS OF A RECIPE
	for (const lineItem of recipe.lineItems) {
		let cheapestPrice: number | undefined;
		let cheapestProduct: Product | undefined;

		//* GET AVAILABLE PRODUCTS FOR AN INGREDIENT AND ITERATE
		for (const product of GetProductsForIngredient(lineItem.ingredient)) {
			//* ITERATE THROUGH PRODUCT SUPPLIERS
			for (const supplierProduct of product.supplierProducts) {
				//* COMPARE SUPPLIER'S PRODUCTS AND GET THE CHEAPEST PRICE
				const price = GetCostPerBaseUnit(supplierProduct);
				if (!cheapestPrice || price < cheapestPrice) {
					cheapestPrice = price;
					cheapestProduct = product;
				}
			}
		}

		//* LOOP THROUGHT THE CHEAPEST PRODUCTS AND COMBINE THE NUTRIENT FACTS AMOUNT
		if (cheapestProduct) {
			for (const nutrientFact of cheapestProduct!.nutrientFacts) {
				const nutFact: NutrientFact = GetNutrientFactInBaseUnits(nutrientFact);
				if (nutFact.nutrientName in nutrientsAtCheapestCost) {
					nutrientsAtCheapestCost[
						nutFact.nutrientName
					].quantityAmount.uomAmount += nutFact.quantityAmount.uomAmount;
				} else {
					nutrientsAtCheapestCost[nutFact.nutrientName] = nutFact;
				}
			}
		}

		//* ADD ALL THE CHEAPEST PRICE VALUE TO GET TOTAL RECIPE COST
		cheapestRecipeCost += cheapestPrice
			? cheapestPrice * lineItem.unitOfMeasure.uomAmount
			: 0;
	}

	//* BASED ON THE PROVIDED EXPECTED RESULT,
	//* SORT THE NUTRIENT FACTS ALPHABETICALLY BY NUTRIENT NAME
	const sortedNutrientsAtCheapestCost: { [key: string]: NutrientFact } = {};
	Object.keys(nutrientsAtCheapestCost)
		.sort()
		.forEach(
			(key) =>
				(sortedNutrientsAtCheapestCost[key] = nutrientsAtCheapestCost[key])
		);

	recipeSummary[recipe.recipeName] = {
		cheapestCost: cheapestRecipeCost,
		nutrientsAtCheapestCost: sortedNutrientsAtCheapestCost,
	};
}

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
